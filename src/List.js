var React = require('react');

import Item from './Item';

var List = React.createClass({

	getInitialState: function () {
		return {
			data: [
				{id: 2, label: "打合せ", selected: false },
				{id: 3, label: "実装", selected: true },
			],
			inputValue: '',
		};
	},

	addItem: function () {

		var l = this.state.inputValue;

		var newId = 0;

		for(var i=0; i<this.state.data.length; i++){
			newId = Math.max(newId, this.state.data[i].id);
		}
		newId++;

		this.state.data.push({id: newId, label: l, selected: false });
		this.setState( {data: this.state.data, inputValue: ''});
	},

	changeItem: function (id) {

	  var nextState = this.state.data.map(function (d) {
	    return {
	      id: d.id,
	      label: d.label,
	      selected: (d.id === id ? !d.selected: d.selected)
	    };
	  });

	  this.setState( {data: nextState });
	},

	removeItem: function (id) {
		for(var i=this.state.data.length-1; i>=0; i--){
			if(this.state.data[i].id === id) this.state.data.splice(i,1)
		}
		this.setState( {data: this.state.data});
	},

	updateInputValue: function(evt) {
		this.setState({ inputValue: evt.target.value });
	},

	render: function() {

		var unfinished = this.state.data.map(function (d,i) {
			return (
				!d.selected ? <Item key={i} label={d.label} checked={d.selected} onChange={this.changeItem.bind(this, d.id)} onRemove={this.removeItem.bind(this, d.id)} /> : ""
			);
		}.bind(this));

		var finished = this.state.data.map(function (d,i) {
			return (
				d.selected ? <Item key={i} label={d.label} checked={d.selected} onChange={this.changeItem.bind(this, d.id)} onRemove={this.removeItem.bind(this, d.id)} /> : ""
			);
		}.bind(this));

		return (
			<div id="todolist">
				<div id="unfinished">
					<h2>unfinished</h2>
					{unfinished}
					<div className="add"><input type="text" ref="inputText" placeholder="type your item" value={this.state.inputValue} onChange={this.updateInputValue} /><button onClick={this.addItem.bind(this)} >add</button></div>
				</div>
				<div id="finished">
					<h2>finished</h2>
					{finished}
				</div>
			</div>
		);
	}

});

module.exports = List;