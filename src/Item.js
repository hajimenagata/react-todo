var React = require('react');

var Item = React.createClass({
	render: function() {
		return (
			<div className="item">
				<label>
					<input type="checkbox" checked={this.props.checked} onChange={this.props.onChange} />{this.props.label}<button onClick={this.props.onRemove} >×</button>
				</label>
			</div>
		);
	}
});

module.exports = Item;