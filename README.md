# Okidev React-demo

## React.jsの特徴

- **Virtual DOM**
root objectだけデータを持ち、そこが変更された時に全てのDOMを書き換える。
→スピード面が心配されるが、差分をチェックし変更があった部分だけ書き換えが発生するため、スピードも早い。
Only the root object has data, and the all DOMs are updated when the data changed.
Updating DOMs is fast because react updates only DOMs which is needed.

- **One way data flow**
Virtual DOMでも触れましたが、データが1箇所で管理されていて、そこの変更を合図に全てのDOMが更新対象になるので、処理の流れが片方向だけで済む。
Since only the root object has data and the DOMs are updated by changing it, the process is simple one way and not complecated.

## セットアップ get started.

```
git clone https://bitbucket.org/hajimenagata/react-todo
cd react-todo
npm install
npm start
```

## ファイル構成 files.

今回、React.jsオフィシャルのhello-worldをcloneし、srcディレクトリ内のファイルを今回のデモ用に作り変えています。(参考:https://facebook.github.io/react/docs/installation.html)
To make this sample, I did fork from hello-world project on React.js official.

計5ファイル
Total 5 files.

src/App.css
src/App.js
src/index.js
src/Item.js
src/List.js

今回はロジックにフォーカスしているので、cssは極力シンプルにしています。
To make simple the code, I didn't focus on the styles very much.

### index.js
```
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
```

index.htmlに読み込まれるファイル。3行目でApp.jsファイルを読み込み、6,7行目で#rootに追加しています。

It is the file index.html loads.
line3 : Load App.js.
line6,7 : Put it to #root


### App.js
```
import React, { Component } from 'react';
import List from './List.js';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
      <List />
      </div>
    );
  }
}

export default App;
```

2行目でList.jsを読み込み、9行目で設置しています。
8行目でしていしているclassName属性は、reactでclass属性として解釈されます。

line2: Load List.js.
line9: Put is to inside div tag which has "App" class.
line8: "className" attribute will be "class" attribute.

### Item.js
```
var React = require('react');

var Item = React.createClass({
  render: function() {
    return (
      <div className="item">
        <label>
          <input type="checkbox" checked={this.props.checked} onChange={this.props.onChange} />{this.props.label}<button onClick={this.props.onRemove} >×</button>
        </label>
      </div>
    );
  }
});

module.exports = Item;
```

ToDoリスト内で生成される行の情報。チェックボックスとラベル、削除ボタンで構成されています。8行目の{}については、後ほど説明します。

A line in the ToDo list which contains a check box, a label and delete button.
In line8, I'll explain later.


### List.js

少し長いので分割して説明します。
It is a bit too large, so I'll split it.

**getInitialState()**
```
  getInitialState: function () {
    return {
      data: [
        {id: 2, label: "打合せ", selected: false },
        {id: 3, label: "実装", selected: true },
      ],
      inputValue: '',
    };
  },
```
ToDoデータ。初期値の設定にも使われます。
dataがToDoデータで、inputValueは入力枠の内容をリアルタイムに保持します。
ToDo data. It is used initial data as well.
'data' is the all of ToDo data, and 'inputValue' is to keep the text which the input field has.

**addItem()**
```
  addItem: function () {

    var l = this.state.inputValue;

    var newId = 0;

    for(var i=0; i<this.state.data.length; i++){
      newId = Math.max(newId, this.state.data[i].id);
    }
    newId++;

    this.state.data.push({id: newId, label: l, selected: false });
    this.setState( {data: this.state.data, inputValue: ''});
  },
```
inputValueの値を使い、dataに新規ToDoを追加します。this.setState実行によって、全てのDOMに更新通知が行く(ようです)。
Adding a new ToDo item using inputValue. By this.setState(), all of DOMs start to update.

**changeItem()**
```
  changeItem: function (id) {

    var nextState = this.state.data.map(function (d) {
      return {
        id: d.id,
        label: d.label,
        selected: (d.id === id ? !d.selected: d.selected)
      };
    });

    this.setState( {data: nextState });
  },
```

チェックボックスの状態が変更された時に呼び出されます。パラメータidはItemが持つユニーク値で、その値とdata内を照合し合致したものが更新されます。
This function is called by changing the checkbox's value in Item. Paramator 'id' is unique, and the todo item in the data which has same id will be updated.

**removeItem()**
```
  removeItem: function (id) {
    for(var i=this.state.data.length-1; i>=0; i--){
      if(this.state.data[i].id === id) this.state.data.splice(i,1)
    }
    this.setState( {data: this.state.data});
  },
```

パラメータidはItemが持つユニーク値で、その値とdata内を照合し合致したものが更新されます。Paramator 'id' is unique, and the todo item in the data which has same id will be removed.

**updateInputValue()**
```
  updateInputValue: function(evt) {
    this.setState({ inputValue: evt.target.value });
  },
```

ToDo追加用のtextfield内の文字をリアルタイムに保持。addItem()内で使用。
(addItem()から直接テキストフィイールドを参照する方法が分からなかったのでこの方法で行いました。jQueryを使わず実現できる方法を知っている方教えて下さい!)
To keep the value in the textfield which is used adding a new line. This value is used in addItem(). I couldn't find how to reference the textfields's value from addItem(), so I took this wey. Please let me know if you know more better way!

**render()**
```
  render: function() {

    var unfinished = this.state.data.map(function (d,i) {
      return (
        !d.selected ? <Item key={i} label={d.label} checked={d.selected} onChange={this.changeItem.bind(this, d.id)} onRemove={this.removeItem.bind(this, d.id)} /> : ""
      );
    }.bind(this));

    var finished = this.state.data.map(function (d,i) {
      return (
        d.selected ? <Item key={i} label={d.label} checked={d.selected} onChange={this.changeItem.bind(this, d.id)} onRemove={this.removeItem.bind(this, d.id)} /> : ""
      );
    }.bind(this));

    return (
      <div id="todolist">
        <div id="unfinished">
          <h2>unfinished</h2>
          {unfinished}
          <div className="add"><input type="text" ref="inputText" placeholder="type your item" value={this.state.inputValue} onChange={this.updateInputValue} /><button onClick={this.addItem.bind(this)} >add</button></div>
        </div>
        <div id="finished">
          <h2>finished</h2>
          {finished}
        </div>
      </div>
    );
  }
```

dataを走査し、unfinishedとfinishedに振り分け、return内の{unfinished}{finished}で描画しています。5行目(11行目)でItemを追加する際、List.js内で定義しているchangeItem()とremoveItem()を送り込むことで繋ぎ込む事ができる。

Sort the todo items to unfinished and finished by iterating data, and then they are rendered. In line5(line11), it puts changeItem() and removeItem() as paramators to a new Item to connect each todo item and functions in List.js.







